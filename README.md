# Install

```shell
curl -s https://gitlab.com/cloudmonitor/cloud/-/raw/main/setup | bash -
```

# Update

```shell
curl -s https://gitlab.com/cloudmonitor/cloud/-/raw/main/setup | bash -s update
```

# Install containers

Run all below as `cloud`

```shell
su cloud
```

## Install Træfik

```shell
cloud install traefik email=my@mail.com
```

## Install Umami

```shell
cloud install umami domain=umami.domain.com
```

# Stop containers

```shell
cloud stop umami
````

### Stop and remove

```shell
cloud stop umami --remove
```

# Start previously created container

```shell
cloud start umami
```

# Update images (added to cron)

```shell
cloud update umami
```
